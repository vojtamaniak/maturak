<?php

namespace App\Repository;

use App\Entity\PlaceGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlaceGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceGroup[]    findAll()
 * @method PlaceGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlaceGroup::class);
    }

//    /**
//     * @return PlaceGroup[] Returns an array of PlaceGroup objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaceGroup
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
