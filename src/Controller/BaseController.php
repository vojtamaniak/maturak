<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BaseController
 * @method User getUser()
 */
class BaseController extends AbstractController
{

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): ObjectManager
    {
        return $this->getDoctrine()->getManager();
    }
}
