<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\EventUser;
use App\Form\EventFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends BaseController
{
    /**
     * @Route("/event/create", name="event_create")
     * @IsGranted("EVENT_CREATE")
     */
    public function createAction(Request $request)
    {
        $eventForm = $this->createForm(EventFormType::class);

        $eventForm->handleRequest($request);

        if($eventForm->isSubmitted() && $eventForm->isValid()){
            $data = $eventForm->getData();

            $event = new Event($data["name"], $data["description"], $data["date"]);

            $this->getEntityManager()->persist($event);

            $eventUser = new EventUser($event, $this->getUser(), EventUser::ROLE_EVENT_ADMIN);
            $this->getEntityManager()->persist($eventUser);
            $this->getEntityManager()->flush();
        }

        return $this->render("event/create.html.twig", [
            "eventForm" => $eventForm->createView()
        ]);
    }

    /**
     * @Route("/event/list", name="event_list")
     * @IsGranted("EVENT_LIST")
     */
    public function listAction(){
        $eventUsers = $this->getDoctrine()->getRepository(EventUser::class)->findByUserId($this->getUser()->getId());

        return $this->render("event/list.html.twig", [
            "eventUsers" => $eventUsers
        ]);
    }

    /**
     * @Route("/event/view/{id}", name="event_view")
     * @IsGranted("EVENT_VIEW", subject="event")
     */
    public function viewAction(Event $event){
        return $this->render("event/view.html.twig", [
            "event" => $event
        ]);
    }
}
