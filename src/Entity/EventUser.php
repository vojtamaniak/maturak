<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventUserRepository")
 */
class EventUser
{
    const ROLE_EVENT_ADMIN = "ROLE_EVENT_ADMIN";
    const ROLE_EVENT_USER = "ROLE_EVENT_USER";

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Event", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;

    public function __construct(Event $event, User $user, string $role = self::ROLE_EVENT_USER)
    {
        $this->event = $event;
        $this->user = $user;
        dump($role);
        if(!$role || !in_array($role, [self::ROLE_EVENT_ADMIN, self::ROLE_EVENT_USER])){
            throw new \InvalidArgumentException("\$role must be a constant from EventUser::ROLE_EVENT_*");
        }

        $this->role = $role;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }
}
