<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaceRepository")
 */
class Place
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaceGroup", inversedBy="places")
     */
    private $placeGroup;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlaceGroup(): ?PlaceGroup
    {
        return $this->placeGroup;
    }

    public function setPlaceGroup(?PlaceGroup $placeGroup): self
    {
        $this->placeGroup = $placeGroup;

        return $this;
    }
}
