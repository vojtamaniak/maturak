<?php

namespace App\Security\Voter;

use App\Entity\Event;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class EventVoter extends BaseVoter
{
    const EDIT = "EVENT_EDIT",
        CREATE = "EVENT_CREATE",
        VIEW = "EVENT_VIEW",
        LIST = "EVENT_LIST";


    protected function supports($attribute, $subject)
    {
        if(in_array($attribute, [self::CREATE, self::LIST]))
            return true;
        return in_array($attribute, [self::CREATE, self::VIEW, self::EDIT])
            && $subject instanceof Event;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /**
         * @var User $user
         */
        $user = $token->getUser();

        switch ($attribute) {
            case self::CREATE:
            case self::LIST:
                if ($this->isAnonymous($user))
                    return false;

                return ($this->hasRole($user, self::ROLE_USER));
            case self::VIEW:
                return true;
        }

        return false;
    }
}
