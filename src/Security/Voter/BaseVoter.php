<?php
/**
 * Created by PhpStorm.
 * User: vojtech
 * Date: 28.10.18
 * Time: 13:35
 */

namespace App\Security\Voter;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class BaseVoter extends Voter
{

    const ROLE_USER = "ROLE_USER",
        ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";

    /**
     * @see Voter
     */
    protected function supports($attribute, $subject)
    {
        return false;
    }

    /**
     * @see Voter
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return false;
    }

    /**
     * Checks if user has desired role
     * @param User $user
     * @param string $role
     * @return bool true if user has desired role, false otherwise
     */
    protected function hasRole(User $user, string $role): bool
    {
        return in_array($role, $user->getRoles());
    }

    /**
     * Checks if user is logged in
     * @param $user
     * @return bool true if user is logged in, false otherwise
     */
    protected function isAnonymous($user)
    {
        return !($user instanceof User);
    }
}