<?php

namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EventControllerTest extends WebTestCase
{
    public function testCreateAction()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', "/event/create");
        $this->assertTrue($client->getResponse()->isRedirect("/login"));
        $client->request("GET", "/event/create", [], [], ["PHP_AUTH_USER" => ""]);
    }
}